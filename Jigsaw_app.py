#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Program name : Jigsaw Project _ Siemens Vulnerability Database.
"""

# ============================================ #
# //             Module imports             // #
# ============================================ #


import database



# ============================================ #
# //      Global variable declarations      // #
# ============================================ #

__author__ = "Dave Caffrey"
__copyright__ = "Copyright 2022, SETU"
__licence__ = "LICENCE, i.e. European Union Public Licence v1.2"
__version__ = "v 1"


# ============================================ #
# //                Functions               // #
# ============================================ #

# -------------------------------------------- #
# //            other() Function            // #
# -------------------------------------------- #


MENU = """

<<<<<------------------------------------------>>>>>

---------------- SETU O.T. app ---------------------

<<<<<------------------------------------------>>>>>

SIEMENS Common Vulnerability Scoring System (CVSS)

Siemens ProductCERT and Siemens CERT
The central expert teams for immediate response
to security threats and issues affecting Siemens
products, solutions, services, or infrastructure.

<<<<<------------------------------------------>>>>>


General  search options:

1) By ID.
2) By CVSS.
3) By DOCUMENT.
4) By VERSION.
5) By DATE.
911) Exit.



Your selection:"""



ADMIN_MENU = """

<<<<<--------------------------------------------->>>>>
------------ ADMINISTRATION PAGE ----------------------
<<<<<--------------------------------------------->>>>>
"""

welcome = "Welcome to the SIEMENS app!"

def color_text(code):
    return "\33[{code}m".format(code=code)



def print_Id_list(heading, SIEMENS):
    print(f"------------------ {heading} HSE ---------------------")
    for Identifier in SIEMENS:
        print(f"ID \t\t\t: {color_text(32)}{Identifier[0]}{color_text(0)}\n"
              f"CVSS \t\t\t: {color_text(32)}{Identifier[1]}{color_text(0)} \n"
              f"DOCUMENT \t\t: {color_text(32)}{Identifier[2]}{color_text(0)}\n"
              f"VERSION \t\t: {color_text(32)}{Identifier[3]}{color_text(0)} \n"
              f"DATE \t\t\t: {color_text(32)}{Identifier[4]}{color_text(0)} \n")
    print("--------------------------------------------------- \n")

def print_Cvss_list(heading, SIEMENS):
    print(f"------------------ {heading} SIEMENS ----------------------")
    for Cvss in SIEMENS:
        print(f"ID \t\t\t: {color_text(32)}{Cvss[0]}{color_text(0)}\n"
              f"CVSS \t\t\t: {color_text(32)}{Cvss[1]}{color_text(0)} \n"
              f"DOCUMENT \t\t: {color_text(32)}{Cvss[2]}{color_text(0)}\n"
              f"VERSION \t\t: {color_text(32)}{Cvss[3]}{color_text(0)} \n"
              f"DATE \t\t\t: {color_text(32)}{Cvss[4]}{color_text(0)} \n")
    print("--------------------------------------------------- \n")

def print_Document_list(heading, SIEMENS):
    print(f"------------------- {heading} SIEMENS ----------------------")
    for Document in SIEMENS:
        print(f"ID \t\t\t: {color_text(32)}{Document[0]}{color_text(0)}\n"
              f"CVSS \t\t\t: {color_text(32)}{Document[1]}{color_text(0)} \n"
              f"DOCUMENT \t\t: {color_text(32)}{Document[2]}{color_text(0)}\n"
              f"VERSION \t\t: {color_text(32)}{Document[3]}{color_text(0)} \n"
              f"DATE \t\t\t: {color_text(32)}{Document[4]}{color_text(0)} \n")
    print("---------------------------------------------------- \n")

def print_Version_list(heading, SIEMENS):
    print(f"------------------- {heading} SIEMENS -----------------------")
    for Version in SIEMENS:
        print(f"ID \t\t\t: {color_text(32)}{Version[0]}{color_text(0)}\n"
              f"CVSS \t\t\t: {color_text(32)}{Version[1]}{color_text(0)} \n"
              f"DOCUMENT \t\t: {color_text(32)}{Version[2]}{color_text(0)}\n"
              f"VERSION \t\t: {color_text(32)}{Version[3]}{color_text(0)} \n"
              f"DATE \t\t\t: {color_text(32)}{Version[4]}{color_text(0)} \n")
    print("---------------------------------------------------- \n")


def print_Date_list(heading, SIEMENS):
    print(f"-------------------- {heading} SIEMENS -----------------------")
    for Date in SIEMENS:
        print(f"ID \t\t\t: {color_text(32)}{Date[0]}{color_text(0)}\n"
              f"CVSS \t\t\t: {color_text(32)}{Date[1]}{color_text(0)} \n"
              f"DOCUMENT \t\t: {color_text(32)}{Date[2]}{color_text(0)}\n"
              f"VERSION \t\t: {color_text(32)}{Date[3]}{color_text(0)} \n"
              f"DATE \t\t\t: {color_text(32)}{Date[4]}{color_text(0)} \n") 
    print("---------------------------------------------------- \n")

def print_admin_list(heading, SIEMENS):
    print(f"-------------------- {heading} SIEMENS ------------------------")
    for ID in SIEMENS:
        print(f"ID name \t: {color_text(32)}{Id[1]}{color_text(0)} \n")
    print("------------------------------------------------------------- \n")

# -------------------------------------------- #
# //            main() Function             // #
# -------------------------------------------- #


database.create_table()

def prompt_search_Id():
    search_term = input("Enter Id: ")
    return database.search_Ids(search_term)

def prompt_search_Cvss():
    search_term = input("Enter CSSV: ")
    return database.search_Cvsss(search_term)

def prompt_search_Document():
    search_term = input("Enter Document?: ")
    return database.search_Documents(search_term)

def prompt_search_Version():
    search_term = input("Enter Version?: ")
    return database.search_Versions(search_term)

def prompt_search_Date():
    search_term = input("Enter Date?: ")
    return database.search_Dates(search_term)

def prompt_insert_admin():
    ID = input("ID : ")
    CVSS = input("CVSS: ")
    Document = input("Document: ")
    Version = input("Version: ")
    Date = input("Date: ")
    database.add_entry(ID, CVSS, Document, Version, Date)


while (user_input := input(MENU)) != "911":
    if user_input == "1":
        SIEMENS = prompt_search_Id()
        if SIEMENS:
            print_Id_list("Id found", SIEMENS)
        else:
           print("Found no Identifier for that search term!")
    elif user_input == "2":
        SIEMENS = prompt_search_Cvss()
        if SIEMENS:
            print_Cvss_list("Cvss found", SIEMENS)
        else:
            print("Found no CVSS for that search term!")           
    elif user_input == "3":
        SIEMENS = prompt_search_Document()
        if SIEMENS:
            print_Document_list("Document match found", SIEMENS)
        else:
            print("No document match found for that search term!")            
    elif user_input == "4":
        SIEMENS = prompt_search_Version()
        if SIEMENS:
            print_Version_list("Version match found", SIEMENS)
        else:
            print("No Version match found for that search term!")
    elif user_input == "5":
        SIEMENS = prompt_search_Date()
        if SIEMENS:
            print_Date_list("Date match found", SIEMENS)
        else:
            print("No Date match found for that search term!")
    elif user_input == "999":
        print(ADMIN_MENU)
        prompt_insert_admin()
    else:
        print("Invalid input, please try again!")
        

       
# End main() function
