#! /usr/bin/env python3
import sqlite3

CREATE_SIEMENS_TABLE = """CREATE TABLE IF NOT EXISTS SIEMENS (ID TEXT , CVSS REAL, Document TEXT, Version TEXT, Date TEXT);"""


SEARCH_ID = """SELECT * FROM SIEMENS WHERE ID LIKE ?;"""
SEARCH_CVSS = """SELECT * FROM SIEMENS WHERE Cvss LIKE ?;"""
SEARCH_DOCUMENT = """SELECT * FROM SIEMENS WHERE Document LIKE ?;"""
SEARCH_VERSION = """SELECT * FROM SIEMENS WHERE Version Like ?;"""
SEARCH_DATE = """SELECT * FROM SIEMENS WHERE Date Like ?;"""
INSERT_ENTRY = "INSERT INTO SIEMENS (ID, CVSS, Document, Version, Date) VALUES (?, ?, ?, ?, ?)"


connection = sqlite3.connect("data.db")


def create_table():
    with connection:
        connection.execute(CREATE_SIEMENS_TABLE)


def search_Ids(search_term):
    with connection:
        cursor = connection.cursor()
        cursor.execute(SEARCH_ID, (f"%{search_term}%",))
        return cursor.fetchall()

def search_Cvsss(search_term):
    with connection:
        cursor = connection.cursor()
        cursor.execute(SEARCH_CVSS, (f"%{search_term}%",))
        return cursor.fetchall()

def search_Documents(search_term):
    with connection:
        cursor = connection.cursor()
        cursor.execute(SEARCH_DOCUMENT, (f"%{search_term}%",))
        return cursor.fetchall()

def search_Versions(search_term):
    with connection:
        cursor = connection.cursor()
        cursor.execute(SEARCH_VERSION, (f"%{search_term}%",))
        return cursor.fetchall()

def search_Dates(search_term):
    with connection:
        cursor = connection.cursor()
        cursor.execute(SEARCH_DATE, (f"%{search_term}%",))
        return cursor.fetchall()

def add_entry(ID, CVSS, Document, Version, Date):
    with connection:
        connection.execute(INSERT_ENTRY, (ID, CVSS, Document, Version, Date))

